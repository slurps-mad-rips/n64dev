import os

textures = {"superluigi.bmp": "rgba8888",
            "smallfont1.bmp": "rgba5551",
            "smallfont2.bmp": "rgba5551",
            "64-grid-template.bmp": "l8"}

for k, v in textures.items():
    print("Converting texture %s to %s" % (k, v))

    no_extension = os.path.splitext(k)[0]
    
    os.system("tex3ds -r -z none -f %s -o ./src/%s.tmp ./src/%s" % (v, k, k))
    os.system("dd if=./src/%s.tmp of=./tex/%s.tex ibs=4 skip=1" % (k, no_extension))
    os.system("rm ./src/%s.tmp" % k)

