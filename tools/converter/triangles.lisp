;;
(defclass triangle-face ()
  ((v1 :initarg :v1 :accessor v1)
   (v2 :initarg :v2 :accessor v2)
   (v3 :initarg :v3 :accessor v3)))

(defmethod print-object ((obj triangle-face) stream)
  (print-unreadable-object (obj stream :type t)
			   (format stream "vertexes (~d ~d ~d)" (v1 obj) (v2 obj) (v3 obj))))
;;

;;
(defclass triangle-vertex ()
  ((vX :initarg :vX :accessor vX)
   (vY :initarg :vY :accessor vY)
   (vZ :initarg :vZ :accessor vZ)))

(defmethod print-object ((obj triangle-vertex) stream)
  (print-unreadable-object (obj stream :type t)
			   (format stream "vX: ~f, vY: ~f, vZ: ~f" (vX obj) (vY obj) (vZ obj))))
