#include "hello.h"

#include "assets/textures/textures.h"

char _64Drive_buffer[512];

volatile int ticks;
volatile bool vbl_flag;


void vblCallback(void)
{
    ticks++;
	vbl_flag = true;

	if(rsp_active)
	{
		sprintf(_64Drive_buffer, "RSP not responding\n");
		_64Drive_putstring(_64Drive_buffer);
	}
	if(waiting_for_rdp)
	{
		sprintf(_64Drive_buffer, "RDP not responding\n");
		_64Drive_putstring(_64Drive_buffer);

		sprintf(_64Drive_buffer, "*** RCP Status ***\n");
		_64Drive_putstring(_64Drive_buffer);

		sprintf(_64Drive_buffer, "PC: %04X | RSP STATUS: %08X\n", MMIO32(0xA4080000), MMIO32(0xA4040010));
		_64Drive_putstring(_64Drive_buffer);

		sprintf(_64Drive_buffer, "RDP CMD_START: %04X | RDP CMD_END: %04X | RDP CMD_CURRENT: %04X\n", MMIO32(0xA4100000), MMIO32(0xA4100004), MMIO32(0xA4100008));
		_64Drive_putstring(_64Drive_buffer);

		sprintf(_64Drive_buffer, "RDP STATUS: %08X\n", MMIO32(0xA410000C));
		_64Drive_putstring(_64Drive_buffer);

		sprintf(_64Drive_buffer, "*** display list:\n");
		_64Drive_putstring(_64Drive_buffer);
		int word = 0;
		for(int i=0; i<16; i++)
		{
			sprintf(_64Drive_buffer, "%04X: ", (i*64));
			_64Drive_putstring(_64Drive_buffer);

			for(int j=0; j<16; j++)
			{
				sprintf(_64Drive_buffer, "%08X ", MMIO32((uint32_t)0xA4000000 + (word*4)));
				_64Drive_putstring(_64Drive_buffer);
				word++;
			}
			_64Drive_putstring("\n");
		}

		_64Drive_putstring("\n");

		while(true) {}
	}
}

void *zbuffer;
void *aligned_zbuffer;

void *display_buffer;
void *aligned_display_buffer;

unsigned int spiral[] = {
    0x000000FF, 0x000100FF, 0x000200FF, 0x000300FF, 0x000400FF, 0x000500FF, 0x000600FF, 0x000700FF, 0x000800FF, 0x000900FF, 0x000A00FF, 0x000B00FF, 0x000C00FF, 0x000D00FF, 0x000E00FF, 0x000F00FF, 
    0xFF10FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0x000000FF, 
    0x002000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x003000FF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x004000FF, 0xFF00FFFF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x005000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x006000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x007000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x008000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x009000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x00A000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x00B000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x00C000FF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x00D000FF, 0xFF00FFFF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0xFF00FFFF, 0x000000FF, 
    0x00E000FF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0xFF00FFFF, 0x000000FF, 
    0x00F000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 0x000000FF, 
};

void LoadTextures()
{
	KLBitmap *fontbig_texture = new KLBitmap;
	TEX_LoadTextureFromCHeader(fontbig_texture, smallfont3, 64, 32, KL_TEX_DEPTH_16BIT, KL_TEX_FORMAT_RGBA);

	KLBitmap *rights_texture = malloc(sizeof(KLBitmap));
	TEX_LoadTextureFromCHeader(rights_texture, rights, 64, 32, KL_TEX_DEPTH_16BIT, KL_TEX_FORMAT_RGBA);

	TextureListEntry *rights = (TextureListEntry *)malloc(sizeof(TextureListEntry));
	strcpy(rights->name, "TRIGHTS");
	rights->info = rights_texture;
	//TM_AppendToTextureList(&textureList, rights);
	textureManager.AppendToTextureList(rights);

	TextureListEntry *fontbig = malloc(sizeof(TextureListEntry));
	strcpy(fontbig->name, "FONTBIG");
	fontbig->info = fontbig_texture;
	//TM_AppendToTextureList(&textureList, fontbig);
	textureManager.AppendToTextureList(fontbig);

	//sprintf(_64Drive_buffer, "Texture list at %p contains %d textures\n", textureList, TM_TextureCount(textureList));
	sprintf(_64Drive_buffer, "Texture list contains %d textures\n", textureManager.TextureCount());
	_64Drive_putstring(_64Drive_buffer);
}

//  Vertex struct is
//     X   Y   Z  0    R    G    B    A  S  T 
//  { -1, -1,  0, 0, 255, 255, 255, 255, 0, 0 }
static BASIC_Vertex test[] = { 
	{ 1, -1, 1, 0, 92, 92, 92, 255, 0, 0 },
	{ -1, -1, -1, 0, 92, 92, 92, 255, 0, 0 },
	{ 1, -1, -1, 0, 92, 92, 92, 255, 0, 0 },

	{ -1, 1, -1, 0, 246, 246, 246, 255, 0, 0 },
	{ 1, 1, 1, 0, 246, 246, 246, 255, 0, 0 },
	{ 1, 1, -1, 0, 246, 246, 246, 255, 0, 0 },

	{ 1, 1, -1, 0, 238, 238, 238, 255, 0, 0 },
	{ 1, -1, 1, 0, 238, 238, 238, 255, 0, 0 },
	{ 1, -1, -1, 0, 238, 238, 238, 255, 0, 0 },

	{ 1, 1, 1, 0, 200, 0, 0, 255, 0, 0 },
	{ -1, -1, 1, 0, 200, 0, 0, 255, 0, 0 },
	{ 1, -1, 1, 0, 200, 0, 0, 255, 0, 0 },

	{ -1, -1, 1, 0, 44, 44, 44, 255, 0, 0 },
	{ -1, 1, -1, 0, 44, 44, 44, 255, 0, 0 },
	{ -1, -1, -1, 0, 44, 44, 44, 255, 0, 0 },

	{ 1, -1, -1, 0, 0, 0, 200, 255, 0, 0 },
	{ -1, 1, -1, 0, 0, 0, 200, 255, 0, 0 },
	{ 1, 1, -1, 0, 0, 0, 200, 255, 0, 0 },

	{ 1, -1, 1, 0, 100, 100, 100, 255, 0, 0 },
	{ -1, -1, 1, 0, 100, 100, 100, 255, 0, 0 },
	{ -1, -1, -1, 0, 100, 100, 100, 255, 0, 0 },

	{ -1, 1, -1, 0, 225, 225, 225, 255, 0, 0 },
	{ -1, 1, 1, 0, 225, 225, 225, 255, 0, 0 },
	{ 1, 1, 1, 0, 225, 225, 225, 255, 0, 0 },

	{ 1, 1, -1, 0, 186, 186, 186, 255, 0, 0 },
	{ 1, 1, 1, 0, 186, 186, 186, 255, 0, 0 },
	{ 1, -1, 1, 0, 186, 186, 186, 255, 0, 0 },

	{ 1, 1, 1, 0, 43, 200, 43, 255, 0, 0 },
	{ -1, 1, 1, 0, 43, 200, 43, 255, 0, 0 },
	{ -1, -1, 1, 0, 43, 200, 43, 255, 0, 0 },

	{ -1, -1, 1, 0, 99, 99, 99, 255, 0, 0 },
	{ -1, 1, 1, 0, 99, 99, 99, 255, 0, 0 },
	{ -1, 1, -1, 0, 99, 99, 99, 255, 0, 0 },

	{ 1, -1, -1, 0, 200, 0, 200, 255, 0, 0 },
	{ -1, -1, -1, 0, 200, 0, 200, 255, 0, 0 },
	{ -1, 1, -1, 0, 200, 0, 200, 255, 0, 0 },
};

void print_debug_info()
{
	Fixed vertex1_x, vertex1_y, vertex1_z, vertex1_w, vertex2_x, vertex2_y, vertex2_z, vertex2_w, vertex3_x, vertex3_y, vertex3_z, vertex3_w;
	vertex1_x = MMIO32((uint32_t)0xA4000960); vertex1_y = MMIO32((uint32_t)0xA4000964); vertex1_z = MMIO32((uint32_t)0xA4000968); vertex1_w = MMIO32((uint32_t)0xA400096C); 
	vertex2_x = MMIO32((uint32_t)0xA4000970); vertex2_y = MMIO32((uint32_t)0xA4000974); vertex2_z = MMIO32((uint32_t)0xA4000978); vertex2_w = MMIO32((uint32_t)0xA400097C);
	vertex3_x = MMIO32((uint32_t)0xA4000980); vertex3_y = MMIO32((uint32_t)0xA4000984); vertex3_z = MMIO32((uint32_t)0xA4000988); vertex3_w = MMIO32((uint32_t)0xA400098C);

	sprintf(_64Drive_buffer, "v1 %08lX %08lX %08lX %08lX\n", vertex1_x, vertex1_y, vertex1_z, vertex1_w);
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "v2 %08lX %08lX %08lX %08lX\n", vertex2_x, vertex2_y, vertex2_z, vertex2_w);
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "v3 %08lX %08lX %08lX %08lX\n", vertex3_x, vertex3_y, vertex3_z, vertex3_w);
	_64Drive_putstring(_64Drive_buffer);

	Fixed dxhdy = ( vertex3_y == vertex1_y ) ? 0 : ( FX_Divide((vertex3_x - vertex1_x), (vertex3_y - vertex1_y)) );
	Fixed dxmdy = ( vertex2_y == vertex1_y ) ? 0 : ( FX_Divide((vertex2_x - vertex1_x), (vertex2_y - vertex1_y)) );
	Fixed dxldy = ( vertex3_y == vertex2_y ) ? 0 : ( FX_Divide((vertex3_x - vertex2_x), (vertex3_y - vertex2_y)) );

	// _____
	// Xh-Yl is the major edge.
	// _____
	// Xm-Yl is the middle edge. 
	// _____
	// Xl-Yl is the low edge.

	int winding = ( vertex1_x * vertex2_y - vertex2_x * vertex1_y ) + ( vertex2_x * vertex3_y - vertex3_x * vertex2_y ) + ( vertex3_x * vertex1_y - vertex1_x * vertex3_y );
	int flip = ( winding > 0 ? 1 : 0 );
	sprintf(_64Drive_buffer, "Expected flip bit: %d\n", flip);
	_64Drive_putstring(_64Drive_buffer);

	Fixed _Xh = FX_Add(vertex1_x, FX_Multiply((vertex1_y & 0xFFFF0000) - vertex1_y, dxhdy));
	Fixed _Xm = FX_Add(vertex1_x, FX_Multiply((vertex1_y & 0xFFFF0000) - vertex1_y, dxmdy));

	sprintf(_64Drive_buffer, "Xh: X0 + (scanlineY - Y0) * DXhDY = %08lX (expected %08lX)\n", MMIO32((uint32_t)0xA40009A8), _Xh);
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "Xm: X0 + (scanlineY - Y0) * DXmDY = %08lX (expected %08lX)\n", MMIO32((uint32_t)0xA40009Ac), _Xm);
	_64Drive_putstring(_64Drive_buffer);

	sprintf(_64Drive_buffer, "Xh %08lX Yh %08lX\n", MMIO32((uint32_t)0xA40009A8), MMIO32((uint32_t)0xA400099C));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "Xm %08lX Ym %08lX\n", MMIO32((uint32_t)0xA40009AC), MMIO32((uint32_t)0xA40009A0));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "Xl %08lX Yl %08lX\n", MMIO32((uint32_t)0xA40009B0), MMIO32((uint32_t)0xA40009A4));
	_64Drive_putstring(_64Drive_buffer);

	// The problem is that we're always using vertices in Y ascending order for calculating X coefficients which isn't necessarily true!
	Fixed leftmost_x = vertex2_x; // debug

	sprintf(_64Drive_buffer, "dxhdy: %08lX (expected: %08lX)\n", MMIO32((uint32_t)0xA40009B4), dxhdy);
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "dxmdy: %08lX (expected: %08lX)\n", MMIO32((uint32_t)0xA40009B8), dxmdy);
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "dxldy: %08lX (expected: %08lX)\n", MMIO32((uint32_t)0xA40009BC), dxldy);
	_64Drive_putstring(_64Drive_buffer);

	sprintf(_64Drive_buffer, "Inverse Depth: %08lX\n", MMIO32((uint32_t)0xA4000A00));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "ZDzDx: %08lX\n", MMIO32((uint32_t)0xA4000A04));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "ZDzDe: %08lX\n", MMIO32((uint32_t)0xA4000A08));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "ZDzDy: %08lX\n", MMIO32((uint32_t)0xA4000A0C));
	_64Drive_putstring(_64Drive_buffer);

	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000A0));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000A4));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000A8));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000AC));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000B0));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000B4));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000B8));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000BC));
	_64Drive_putstring(_64Drive_buffer);
}

float scale_factor_x = 1.0;
float scale_factor_y = 1.0;

int vert = 100, vert2 = 40;
int horiz = 40, horiz2 = 200;
int add_to_vert = 1, add_to_vert2 = 1;
int add_to_horiz = 1, add_to_horiz2 = -1;

void TestSecondDisplayList(display_context_t context)
{
	if(vert >= 180)
	{
		add_to_vert = -1;
	}
	else if(vert <= 20)
	{
		add_to_vert = 1;
	}

	if(horiz >= 200)
	{
		add_to_horiz = -1;
	}
	else if(horiz <= 20)
	{
		add_to_horiz = 1;
	}

	if(vert2 >= 180)
	{
		add_to_vert2 = -1;
	}
	else if(vert2 <= 20)
	{
		add_to_vert2 = 1;
	}

	if(horiz2 >= 240)
	{
		add_to_horiz2 = -1;
	}
	else if(horiz2 <= 20)
	{
		add_to_horiz2 = 1;
	}
	
	vert = vert + add_to_vert;
	horiz = horiz + add_to_horiz;

	vert2 = vert2 + add_to_vert2;
	horiz2 = horiz2 + add_to_horiz2;

	display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

	rdp_attach_display(&display_list_current, context);
	rdp_set_primitive_color(&display_list_current, 0xFFFFFFFF);
	rdp_set_combine_mode(&display_list_current, CC_C0_RGB_MUL_ZERO_COLOR|CC_C1_RGB_MUL_ZERO_COLOR|CC_C0_RGB_ADD_TEXEL0_COLOR|CC_C1_RGB_ADD_TEXEL0_COLOR|
		CC_C0_ALPHA_MUL_ZERO|CC_C0_ALPHA_ADD_TEXEL0|CC_C1_ALPHA_MUL_ZERO|CC_C1_ALPHA_ADD_TEXEL0);
	rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_1CYCLE|MODE_ALPHA_COMPARE_EN|MODE_SHARPEN_TEX_EN);
	rdp_sync(&display_list_current, SYNC_PIPE);

	/*
	rdp_load_texture(&display_list_current, TEXSLOT_0, 0, MIRROR_DISABLED, TM_GetTextureByName(textureList, "TRIGHTS")->info);
	rdp_draw_textured_rectangle_scaled(&display_list_current, TEXSLOT_0, 96, 76, 96+128, 76+64, 2.0, 2.0, 0, 0);
	rdp_sync(&display_list_current, SYNC_PIPE);
	rdp_draw_textured_rectangle_scaled(&display_list_current, TEXSLOT_0, horiz2, vert2, horiz2+64, vert2+48, 1.0, 1.5, 0, 0);
	rdp_sync(&display_list_current, SYNC_PIPE);
	*/

	DL_MAC_WriteStringToScreen(&display_list_current, true, "I AM A NINTENDO AND I SAY", 20, 180);

	rdp_sync(&display_list_current, SYNC_FULL);
	rdp_end_display_list(&display_list_current);
}

void WriteDisplayListToRCP(display_context_t context)
{
	// Build a display list in the context given. Transfer it to the RDP once built.
	display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

	rdp_set_default_clipping(&display_list_current);
	rdp_set_default_clipping(&display_list_current);
	rdp_set_default_clipping(&display_list_current);

	// Background
	rdp_attach_display(&display_list_current, context);
	rdp_set_fill_mode(&display_list_current);
	rdp_set_primitive_color(&display_list_current, 0x0000000FF);
	rdp_draw_filled_rectangle(&display_list_current, 0, 0, 319, 239);
	rdp_sync(&display_list_current, SYNC_PIPE);

	rdp_attach_display(&display_list_current, context);
	rdp_set_combine_mode(&display_list_current, CC_C0_RGB_MUL_ZERO_COLOR|CC_C1_RGB_MUL_ZERO_COLOR|CC_C0_RGB_ADD_SHADE_COLOR|CC_C1_RGB_ADD_SHADE_COLOR);
	rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_1CYCLE|MODE_CVG_DEST_CLAMP|MODE_IMAGE_READ_EN|MODE_Z_COMPARE_EN|MODE_Z_UPDATE_EN|RM_AA_ZB_OPA_SURF(1)|RM_AA_ZB_OPA_SURF(2));
	rdp_set_z_image(&display_list_current, aligned_zbuffer);

	rdp_set_primitive_color(&display_list_current, 0xFFFFFFFF);
	rdp_sync(&display_list_current, SYNC_PIPE);

	BASIC_LoadTransformationMatrix(&display_list_current, (uint32_t *)&transformation_matrix);
	BASIC_LoadVertexBuffer(&display_list_current, test, 0, 15);
	// BASIC_DrawTriangle(&display_list_current, 0, 1, 2);
	// BASIC_DrawTriangle(&display_list_current, 3, 4, 5);
	 BASIC_DrawTriangle(&display_list_current, 6, 7, 8);
	BASIC_DrawTriangle(&display_list_current, 9, 10, 11); // one
	// BASIC_DrawTriangle(&display_list_current, 12, 13, 14);

	BASIC_LoadVertexBuffer(&display_list_current, &(test[15]), 0, 15);
	BASIC_DrawTriangle(&display_list_current, 0, 1, 2);
	// BASIC_DrawTriangle(&display_list_current, 3, 4, 5);
	// BASIC_DrawTriangle(&display_list_current, 6, 7, 8);
	// BASIC_DrawTriangle(&display_list_current, 9, 10, 11);
 	BASIC_DrawTriangle(&display_list_current, 12, 13, 14); // two

	BASIC_LoadVertexBuffer(&display_list_current, &(test[30]), 0, 6);
	// BASIC_DrawTriangle(&display_list_current, 0, 1, 2);
	BASIC_DrawTriangle(&display_list_current, 3, 4, 5);

	rdp_sync(&display_list_current, SYNC_FULL);

	rdp_end_display_list(&display_list_current);
}

void wait_frames(int num)
{
	int target_ticks = ticks + num;

	while(ticks < target_ticks)
	{
		//sprintf(_64Drive_buffer, "Waiting for frame %d. Current frame is %d\n", target_ticks, ticks);
		//_64Drive_putstring(_64Drive_buffer);	
	}
}

void clear_rsp_buffers()
{
	for(int i=0; i<512; i++)
	{
		MMIO32(SP_DMEM_BASE + (4*i)) = 0;
	}
}

int main()
{
	RSP_InitSegmentsList();
	RSP_CopySegmentAddressesToDMEM();
	RSP_LoadBootSegment();

	rsp_active = false;
	waiting_for_rdp = false;

	// Wait for the 64drive to become available.
	// If it never does, assume we don't have one hooked up.
	_64Drive_wait();
	ticks = 0;

	sprintf(_64Drive_buffer, "\n\n*** N64 Boot ***\n");
	_64Drive_putstring(_64Drive_buffer);

	//textureList = NULL;

	_64Drive_putstring("Initializing MVP matrices\n");

	// Start building matrices that we need.
	klPerspective(&perspective_matrix, 1, 500);
	klUnitMatrix(&view_matrix);
	klUnitMatrix(&model_matrix);

	// Make sure we aren't caching these values.
	data_cache_hit_writeback_invalidate(&perspective_matrix, sizeof(FX_Matrix44));
	data_cache_hit_writeback_invalidate(&view_matrix, sizeof(FX_Matrix44));
	data_cache_hit_writeback_invalidate(&model_matrix, sizeof(FX_Matrix44));

	_64Drive_putstring("Initializing interrupts.\n");

	init_interrupts();
	register_VI_handler(vblCallback); // VBlank handler
	_64Drive_putstring("VBL handler installed\n");

	// Get a context and blank the screen.
	_64Drive_putstring("Initializing display: 320x240, 32bpp.\n");
	display_init(RESOLUTION_320x240, DEPTH_32_BPP, 1, GAMMA_NONE, ANTIALIAS_RESAMPLE);
	set_VI_interrupt(1, 360);

	_64Drive_putstring("Initializing RDP.\n");
	rdp_init();
	_64Drive_putstring("Initializing RSP.\n");
	rsp_init();
	_64Drive_putstring("Initializing timers.\n");
	timer_init();
	_64Drive_putstring("Initializing controllers.\n");
	controller_init();
	_64Drive_putstring("Initializing DFS.\n");
	if(dfs_init(DFS_DEFAULT_LOCATION) != DFS_ESUCCESS)
	{
		_64Drive_putstring("Failed to initialize DFS!\n");
		while(true) {};
	}

	LoadTextures();

	wait_frames(60);

	// Acquire the framebuffer.
	_64Drive_putstring("Acquiring a framebuffer and clearing it.\n");
	int disp_context = display_lock();
	uint32_t *framebuffer = __get_buffer(disp_context);

	bool enable_controls = true;
	bool enable_rotation = true;

	int degrees = 0;
	float pos_x = 0, pos_y = 0, pos_z = -10;
	struct controller_data cdata;

	_64Drive_putstring("Beginning frame loop\n");

	// Copy the command jump table from IMEM to DMEM.
	for(int i=0; i<128; i++)
	{
		MMIO32(BASIC_JumpTableDMEM + (i*4)) = MMIO32(BASIC_JumpTableIMEM + (i*4));
	}

	display_buffer = malloc(320*240*4 + 63);
	aligned_display_buffer = ALIGN_64BYTE( UNCACHED_ADDR( zbuffer ) );

	zbuffer = malloc(320*240*2 + 63);
	aligned_zbuffer = ALIGN_64BYTE( UNCACHED_ADDR( zbuffer ) );
	sprintf(_64Drive_buffer, "Aligned ZBuffer: %08X\n", aligned_zbuffer);
	_64Drive_putstring(_64Drive_buffer);

	/* frame loop */
	while(true)
	{
		// Start of a new frame.
		vbl_flag = false;

		// Read the controller state.
		controller_scan(); // Scan the attached controllers.
		cdata = get_keys_held();

		if(enable_controls)
		{
			pos_x += (cdata.c[0].x * 0.002);
			pos_z += (cdata.c[0].y * -0.002);

			if(cdata.c[0].C_down) scale_factor_y += 0.01;
			if(cdata.c[0].C_up) scale_factor_y -= 0.01;
			
			if(cdata.c[0].C_left) scale_factor_x -= 0.01;
			if(cdata.c[0].C_right) scale_factor_x += 0.01;

			//if(cdata.c[0].A) degrees++;
			//if(cdata.c[0].B) degrees--;
		}

		if(enable_rotation) degrees = (degrees + 1) % 360;

		FX_Matrix44 view_rotation_x, view_rotation_y, view_rotation_z, view_translation;

		klTranslation(UncachedAddr(&view_translation), FX_FromFloat(pos_x), FX_FromInt(0), FX_FromFloat(pos_z));
		klRotationDegreesX(UncachedAddr(&view_rotation_x), 0);
		klRotationDegreesY(UncachedAddr(&view_rotation_y), 0);
		klRotationDegreesZ(UncachedAddr(&view_rotation_z), degrees);

		klUnitMatrix(UncachedAddr(&view_matrix));

		klMatrixMultiply(UncachedAddr(&view_translation), UncachedAddr(&view_matrix), UncachedAddr(&view_matrix));

		klMatrixMultiply(UncachedAddr(&view_rotation_z), UncachedAddr(&view_matrix), UncachedAddr(&view_matrix));
		klMatrixMultiply(UncachedAddr(&view_rotation_y), UncachedAddr(&view_matrix), UncachedAddr(&view_matrix));
		klMatrixMultiply(UncachedAddr(&view_rotation_x), UncachedAddr(&view_matrix), UncachedAddr(&view_matrix));

		klMatrixMultiply(UncachedAddr(&view_matrix), UncachedAddr(&perspective_matrix), UncachedAddr(&transformation_matrix));

		DL_ClearDepthBuffer(disp_context);
		run_ucode();
		wait_for_rsp();
		wait_for_rdp();

		WriteDisplayListToRCP(disp_context);
		run_ucode();
		wait_for_rsp();
		wait_for_rdp();

		TestSecondDisplayList(disp_context);
		run_ucode();
		wait_for_rsp();
		wait_for_rdp();

		// Wait for a vblank.
		while(vbl_flag == false) {};

		display_show(disp_context);

		/*
		sprintf(_64Drive_buffer, "*** display list ***\n");
		_64Drive_putstring(_64Drive_buffer);
		int word = 0;
		for(int i=0; i<16; i++)
		{
			for(int j=0; j<16; j++)
			{
				sprintf(_64Drive_buffer, "%08X ", MMIO32((uint32_t)0xA4000000 + (word*4)));
				_64Drive_putstring(_64Drive_buffer);
				word++;
			}
			_64Drive_putstring("\n");
		}

		_64Drive_putstring("\n");
		*/

		//print_debug_info();
		//while(true){};
	}
	
	while(true) {};
}
