#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <libdragon.h>
#include <string.h>

#include "fixed.h"
#include "kategl/matrix.h"
#include "mmio.h"
#include "regs.h"
#include "3d.h"
#include "64drive.h"
#include "rsp_overlay.h"

#include "rcp/sync.h"
#include "microcode/basic.h"
#include "display_lists/display_lists.h"
#include "texture/manager.h"
#include "texture/loader.h"
#include "kategl/kategl.h"

#define DEBUG_ENABLED

#define __get_buffer( x ) __safe_buffer[(x)-1]

static FX_Matrix44 perspective_matrix __attribute__((aligned(8)));
static FX_Matrix44 view_matrix __attribute__((aligned(8)));
static FX_Matrix44 model_matrix __attribute__((aligned(8)));
static FX_Matrix44 transformation_matrix __attribute__((aligned(8)));

display_list_t my_cool_display_list[1024] __attribute__((aligned(8)));
display_list_t *DMEM_display_list = (display_list_t *)0xA4000000;

/* Raycaster microcode symbols. */
extern const char basic_ucode_start;
extern const char basic_ucode_end;

extern const char vertex_ucode_start;
extern const char vertex_ucode_end;

extern void *__safe_buffer[];

