        .macro      PUSH reg
        sub         $sp, $sp, 4
        sw          \reg, ($sp)
        .endm

        .macro      POP reg
        lw          \reg, ($sp)
        addiu       $sp, $sp, 4
        .endm
        
        
         .macro      DumpScalarRegisters
        sw          $0,0xC00
        sw          $1,0xC04
        sw          $2,0xC08
        sw          $3,0xC0C
        sw          $4,0xC10
        sw          $5,0xC14
        sw          $6,0xC18
        sw          $7,0xC1C
        sw          $8,0xC20
        sw          $9,0xC24
        sw          $10,0xC28
        sw          $11,0xC2C
        sw          $12,0xC30
        sw          $13,0xC34
        sw          $14,0xC38
        sw          $15,0xC3C
        sw          $16,0xC40
        sw          $17,0xC44
        sw          $18,0xC48
        sw          $19,0xC4C
        sw          $20,0xC50
        sw          $21,0xC54
        sw          $22,0xC58
        sw          $23,0xC5C
        sw          $24,0xC60
        sw          $25,0xC64
        sw          $26,0xC68
        sw          $27,0xC6C
        sw          $28,0xC70
        sw          $29,0xC74
        sw          $30,0xC78
        sw          $31,0xC7C
        .endm

        .macro      DumpVectorRegisters
        li          $8,0xC80

        # Matrix A
        # Integer
        sqv         VEC0, 0, 0, S_T0
        sqv         VEC1, 0, 1, S_T0
        # Fraction
        sqv         VEC6, 0, 2, S_T0
        sqv         VEC7, 0, 3, S_T0

        # Matrix B
        # Integer
        sqv         VEC2, 0, 4, S_T0
        sqv         VEC3, 0, 5, S_T0
        # Fraction
        sqv         VEC8, 0, 6, S_T0
        sqv         VEC9, 0, 7, S_T0

        # Matrix R
        # Integer
        sqv         VEC4, 0, 8, S_T0
        sqv         VEC5, 0, 9, S_T0
        # Fraction
        sqv         VEC10, 0, 10, S_T0
        sqv         VEC11, 0, 11, S_T0

        
        .endm       
