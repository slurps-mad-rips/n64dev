# You can't divide matrices but I'm keeping this around as an example of
# how to divide fixed-point numbers.
MatrixAB_Divide:
# Divide Matrix A by Matrix B, storing the result in Matrix R.

# Load VEC31 with the constant 2.
        li          $t0,0x00020002
        sw          $t0,TempWord1
        li          $t0,TempWord1
        llv         VEC31, 0, 0, S_T0
        llv         VEC31, 4, 0, S_T0
        llv         VEC31, 8, 0, S_T0
        llv         VEC31, 12, 0, S_T0

        li          $t0,0x0
        sw          $t0,TempWord1
        li          $t0,TempWord1
        llv         VEC30, 0, 0, S_T0
        llv         VEC30, 4, 0, S_T0
        llv         VEC30, 8, 0, S_T0
        llv         VEC30, 12, 0, S_T0
        
# Calculate the reciprocal of Matrix B.
# VEC12 = bres_int | VEC13 = bres_frac
# VEC30 = dev_null | VEC31 = vconst        

        # Matrix B
        vrcph       VEC12, 0, VEC2, 0
        vrcpl       VEC13, 0, VEC8, 0
        vrcph       VEC12, 0, VEC30, 0

        vrcph       VEC12, 1, VEC2, 0
        vrcpl       VEC13, 1, VEC8, 0
        vrcph       VEC12, 1, VEC30, 0

        vrcph       VEC12, 2, VEC2, 0
        vrcpl       VEC13, 2, VEC8, 0
        vrcph       VEC12, 2, VEC30, 0

        vrcph       VEC12, 3, VEC2, 0
        vrcpl       VEC13, 3, VEC8, 0
        vrcph       VEC12, 3, VEC30, 0

        vrcph       VEC12, 4, VEC2, 0
        vrcpl       VEC13, 4, VEC8, 0
        vrcph       VEC12, 4, VEC30, 0

        vrcph       VEC12, 5, VEC2, 0
        vrcpl       VEC13, 5, VEC8, 0
        vrcph       VEC12, 5, VEC30, 0

        vrcph       VEC12, 6, VEC2, 0
        vrcpl       VEC13, 6, VEC8, 0
        vrcph       VEC12, 6, VEC30, 0

        vrcph       VEC12, 7, VEC2, 0
        vrcpl       VEC13, 7, VEC8, 0
        vrcph       VEC12, 7, VEC30, 0

        # Shift all operands left by 1.
        vmudn       VEC13, VEC13, VEC31, 2
        vmadm       VEC12, VEC12, VEC31, 2
        vmadn       VEC13, VEC30, VEC30, 0
        
        # Calculate the result now that we have reciprocals.
        vmudl       VEC10, VEC13, VEC6, 0
        vmadm       VEC4,  VEC12, VEC6, 0
        vmadn       VEC4,  VEC13, VEC0, 0
        vmadh       VEC4,  VEC12, VEC0, 0
        vmadn       VEC10, VEC13, VEC6, 0               

        jr          $ra
        nop

MatrixAB_Multiply:
# Multiply Matrix A by Matrix B, storing the result in Matrix R.
        # Clear the result matrix
        li      $t0, 0x0000
        li      $t1, MatrixR
        li      $t2, 30

        li      $t3, MatrixA
        li      $t4, MatrixB

1:      # loop to set matrix to all 0s
        sh      $t0, ($t1)
        addi    $t2, -2
        addi    $t1, 2
        bgez    $t2, 1b
        nop

        li          $a0, MatrixR
        lqv         VEC4, 0, 0, S_A0
        lqv         VEC5, 0, 0, S_A0
        lqv         VEC10, 0, 0, S_A0
        lqv         VEC11, 0, 0, S_A0

        # debugging: do this without the vector unit so I can figure out the logic
        sqv     VEC0, 0, 0, S_T3
        srv     VEC0, 0, 0, S_T3
        sqv     VEC1, 0, 1, S_T3
        srv     VEC1, 0, 1, S_T3
        sqv     VEC6, 0, 2, S_T3
        srv     VEC6, 0, 2, S_T3
        sqv     VEC7, 0, 3, S_T3
        srv     VEC7, 0, 3, S_T3

        sqv     VEC2, 0, 0, S_T4
        srv     VEC2, 0, 0, S_T4
        sqv     VEC3, 0, 1, S_T4
        srv     VEC3, 0, 1, S_T4
        sqv     VEC8, 0, 2, S_T4
        srv     VEC8, 0, 2, S_T4
        sqv     VEC9, 0, 3, S_T4
        srv     VEC9, 0, 3, S_T4

        # Perform the multiply
        # We need to use the vector unit to do multiplies.

# Integer rows are stored in  A: VEC0-VEC1 B: VEC2-VEC3 R: VEC4-VEC5
# Fraction rows are stored in A: VEC6-VEC7 B: VEC8-VEC9 R: VEC10-VEC11

# Temporary row 1: integer VEC20-VEC21, fraction VEC22-VEC23
# Temporary row 2: integer VEC24-VEC25, fraction VEC26-VEC27

        # Produce some integer results.
        #vmudl       VEC10, VEC6, VEC8, V_ELEMENT0
        #vmadm       VEC4, VEC0, VEC8, V_ELEMENT0
        #vmadn       VEC10, VEC6, VEC2, V_ELEMENT0
        #vmadh       VEC4, VEC0, VEC2, V_ELEMENT0

        # Construct a vector that we use to produce one cell result.
        # a11*b11 + a12*b21 + a13*b31 + a14*b41

        # Produce cell 1,1 of the final matrix.
        # int
        vmov    VEC20, V_ELEMENT0, VEC0, V_ELEMENT0
        vmov    VEC24, V_ELEMENT0, VEC2, V_ELEMENT0

        vmov    VEC20, V_ELEMENT1, VEC0, V_ELEMENT1
        vmov    VEC24, V_ELEMENT1, VEC2, V_ELEMENT1

        vmov    VEC20, V_ELEMENT2, VEC0, V_ELEMENT2
        vmov    VEC24, V_ELEMENT2, VEC3, V_ELEMENT0

        vmov    VEC20, V_ELEMENT3, VEC0, V_ELEMENT3
        vmov    VEC24, V_ELEMENT3, VEC3, V_ELEMENT4

        # frac
        vmov    VEC22, V_ELEMENT0, VEC6, V_ELEMENT0
        vmov    VEC26, V_ELEMENT0, VEC8, V_ELEMENT0

        vmov    VEC22, V_ELEMENT1, VEC6, V_ELEMENT1
        vmov    VEC26, V_ELEMENT1, VEC8, V_ELEMENT1

        vmov    VEC22, V_ELEMENT2, VEC6, V_ELEMENT2
        vmov    VEC26, V_ELEMENT2, VEC9, V_ELEMENT0

        vmov    VEC22, V_ELEMENT3, VEC6, V_ELEMENT3
        vmov    VEC26, V_ELEMENT3, VEC9, V_ELEMENT4

        # 16.16 multiplication
        vmudl   VEC10, VEC24, VEC26, 0
        vmadm   VEC4, VEC20, VEC26, 0
        vmadn   VEC10, VEC24, VEC22, 0
        vmadh   VEC4, VEC20, VEC24, 0

        # Need to send to an alternate vector to calculate the result or else
        # the intermediate result corrupts the original operands
        vaddc   VEC13, VEC10, VEC10, V_ELEMENT1
        vadd    VEC12, VEC4, VEC4, V_ELEMENT1
        vaddc   VEC13, VEC13, VEC10, V_ELEMENT2
        vadd    VEC12, VEC12, VEC4, V_ELEMENT2
        vaddc   VEC13, VEC13, VEC10, V_ELEMENT3
        vadd    VEC12, VEC12, VEC4, V_ELEMENT3

        # Write cell 1,1 to DMEM
        li      $a0, MatrixR
        ssv     VEC12, 0, 0, S_A0
        ssv     VEC13, 0, 16, S_A0

        # produce cell 1,1
#        li      $a0, MatrixR
#        sqv     VEC4, 0, 0, S_A0
#        srv     VEC4, 0, 0, S_A0
#        sqv     VEC5, 0, 1, S_A0
#        srv     VEC5, 0, 1, S_A0
#        sqv     VEC10, 0, 2, S_A0
#        srv     VEC10, 0, 2, S_A0
#        sqv     VEC11, 0, 3, S_A0
#        srv     VEC11, 0, 3, S_A0

        jr          $ra
        nop