#pragma once

#ifndef BASIC_H__
#define BASIC_H__

#include <stdint.h>

#define ADVANCE_DISPLAY_LIST_PTR ( *list = (display_list_t *)(*list + 1) )

#define SP_DMEM_BASE	0xA4000000
#define SP_IMEM_BASE	0xA4001000

/* Symbol locations for the Basic Microcode. */
#define BASIC_RDPCommandRingBuffer ( SP_DMEM_BASE + 0x000 )
#define BASIC_InputDisplayList ( SP_DMEM_BASE + 0x400 )

#define BASIC_MatrixA ( SP_DMEM_BASE + 0x800 )
#define BASIC_MatrixB ( SP_DMEM_BASE + 0x840 )
#define BASIC_MatrixR ( SP_DMEM_BASE + 0x880 )

#define BASIC_Matrix_IntOffset ( 0x00 )
#define BASIC_Matrix_FracOffset ( 0x20 )

#define BASIC_MatrixProjection ( SP_DMEM_BASE + 0x8C0 )
#define BASIC_MatrixView ( SP_DMEM_BASE + 0x8E0 )

#define BASIC_JumpTableIMEM ( SP_IMEM_BASE + 0xE00 )
#define BASIC_JumpTableDMEM ( SP_DMEM_BASE + 0xE00 )

#define BASIC_VertexTable ( SP_DMEM_BASE + 0xD00 )

#pragma pack(push,1)
typedef struct BASIC_Vertex {
	int16_t 	x, y, z;	// Vertex coordinates (signed 16-bit integer)
	int16_t     w;
	uint8_t		r, g, b, a; // Colors and alpha (0-255, unsigned 8-bit)
	uint16_t 	s, t;		// Texture coordinates (S10.5 fixed-point)
} BASIC_Vertex;				// Length: 16 bytes
#pragma pack(pop)

/* Function macros, etc. */
static void BASIC_LoadProjectionMatrix( display_list_t **list, uint32_t *matrix_ptr )
{
    list[0]->words.hi = 0xC0000000 | ((uint32_t)matrix_ptr & 0x00FFFFFF);
    list[0]->words.lo = 0;
    ADVANCE_DISPLAY_LIST_PTR; 
}

static void BASIC_LoadVertexBuffer( display_list_t **list, BASIC_Vertex *vertex_ptr, uint8_t start, uint8_t count )
{
	/*
	HI: $41AAAAAA
	LO: $SSCC0000

	A = vertex_ptr
	S = start
	C = count
	*/

	// vertex_ptr: RDRAM address of the vertex list we're retrieving
	// start: vertex ID that we copy the incoming list to in the RSP vertex buffer
	// count: number of vertices to copy
	list[0]->words.hi = 0xC1000000 | ((uint32_t)vertex_ptr & 0x00FFFFFF);
	list[0]->words.lo = (start << 24) | (count << 16);
	ADVANCE_DISPLAY_LIST_PTR;
}

// Set up a filled triangle with vertices v1,v2,v3 from the buffer.
static void BASIC_DrawTriangle( display_list_t **list, uint8_t v1, uint8_t v2, uint8_t v3 )
{
	list[0]->words.hi = 0xC2000000 | (v1 << 16) | (v2 << 8) | v3;
    MMIO32(((uint32_t)list[0]) + 4) = 0xD000000F;
	//MMIO32(((uint32_t)list[0]) + 4) = 0x8000000F;
	ADVANCE_DISPLAY_LIST_PTR;
}

static void BASIC_LoadModelMatrix( display_list_t **list, uint32_t *matrix_ptr )
{
    list[0]->words.hi = 0xC3000000 | ((uint32_t)matrix_ptr & 0x00FFFFFF);
    list[0]->words.lo = 0;
    ADVANCE_DISPLAY_LIST_PTR; 
}

static void BASIC_LoadViewMatrix( display_list_t **list, uint32_t *matrix_ptr )
{
    list[0]->words.hi = 0xC4000000 | ((uint32_t)matrix_ptr & 0x00FFFFFF);
    list[0]->words.lo = 0;
    ADVANCE_DISPLAY_LIST_PTR; 
}

static void BASIC_MVPMultiply( display_list_t **list)
{
    list[0]->words.hi = 0xC5000000;
    list[0]->words.lo = 0;
    ADVANCE_DISPLAY_LIST_PTR; 
}

static void BASIC_LoadTransformationMatrix( display_list_t **list, uint32_t *matrix_ptr )
{
    list[0]->words.hi = 0xC6000000 | ((uint32_t)matrix_ptr & 0x00FFFFFF);
    list[0]->words.lo = 0;
    ADVANCE_DISPLAY_LIST_PTR; 
}

#endif
