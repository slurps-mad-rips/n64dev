#include "texture/manager.h"

TextureManager textureManager;

void TextureManager::AppendToTextureList(TextureListEntry *texture)
{
    texture->next = NULL;

    if(list == NULL)
    {
        list = texture;
    }
    else
    {
        TextureListEntry *current = list;

        while(current->next != NULL)
        {
            current = current->next;
        }
        current->next = texture;
    }
    
}

int TextureManager::TextureCount()
{
    if(list == NULL)
    {
        return -1; // uninitialized list
    }
    else
    {
        TextureListEntry *current = list;
        int count = 0;

        while(current != NULL)
        {
            current = current->next;
            count++;
        }

        return count;
    }
}

TextureListEntry* TextureManager::GetTextureByName(char *seekname)
{
    if(list == NULL)
    {
        return NULL;
    }
    else
    {
        TextureListEntry *current = list;
        while(current != NULL)
        {
            if(strcmp(current->name, seekname) == 0) return current;

            current = current->next;
        }

        // Didn't find it. Write an error to the console.
        char buf[64];
        sprintf(buf, "Texture '%s' not found in texture list %p\n", seekname, list);
        _64Drive_putstring(buf);
        return NULL;
    }
}

// void TM_AppendToTextureList(TextureListEntry **head, TextureListEntry *new_entry)
// {
//     new_entry->next = NULL;

//     if(*head == NULL)
//     {
//         *head = new_entry;
//     }
//     else
//     {
//         TextureListEntry *current = *head;

//         while(current->next != NULL)
//         {
//             current = current->next;
//         }

//         current->next = new_entry;
//     }
    
// }

// int TM_TextureCount(TextureListEntry *list)
// {
//     if(list == NULL)
//     {
//         return -1; // uninitialized list
//     }
//     else
//     {
//         TextureListEntry *current = list;
//         int count = 0;

//         while(current != NULL)
//         {
//             current = current->next;
//             count++;
//         }

//         return count;
//     }
// }

// TextureListEntry *TM_GetTextureByName(TextureListEntry *list, char *seekname)
// {
//     if(list == NULL)
//     {
//         return NULL;
//     }
//     else
//     {
//         TextureListEntry *current = list;
//         while(current != NULL)
//         {
//             if(strcmp(current->name, seekname) == 0) return current;

//             current = current->next;
//         }

//         // Didn't find it. Write an error to the console.
//         char buf[64];
//         sprintf(buf, "Texture '%s' not found in texture list %p\n", seekname, list);
//         _64Drive_putstring(buf);
//         return NULL;
//     }
    
// }