#include "loader.h"

void TEX_LoadTextureFromFS(KLBitmap *tex, char *filename, uint16_t width, uint16_t height, KLTextureDepth texture_size, KLTextureFormat texture_format)
{
    char _64Drive_buffer[256];

	sprintf(_64Drive_buffer, "Loading texture to %08X: %s\n", tex, filename);
	_64Drive_putstring(_64Drive_buffer);

	int depth;
	
	switch(texture_size)
	{
		case KL_TEX_DEPTH_8BIT:
			depth = 1;
			break;
		case KL_TEX_DEPTH_16BIT:
			depth = 2;
			break;
		case KL_TEX_DEPTH_32BIT:
			depth = 4;
			break;
		default:
			_64Drive_putstring("Illegal texture size in TEX_LoadTextureFromFS!\n");
			while(true) {};
			break;
	}

	int byte_length = width * height * depth;
	sprintf(_64Drive_buffer, "Texture is %d bytes\n", byte_length);
	_64Drive_putstring(_64Drive_buffer);

	FILE *texture_fs = fopen(filename, "rb");
	uint8_t *texture_data = malloc(byte_length);
	int bytes = fread(texture_data, depth, width*height, texture_fs);

	sprintf(_64Drive_buffer, "Read %d bytes. %02X%02X%02X%02X\n", bytes, texture_data[0], texture_data[1], texture_data[2], texture_data[3]);
	_64Drive_putstring(_64Drive_buffer);

	tex->data = texture_data;
	tex->bitdepth = depth;
	tex->size.y = height;
	tex->size.x = width;
	tex->format = texture_format;
	tex->pixel_size = texture_size;

	//8-bit pixels: 64 bits is 8 pixels
	//16-bit pixels: 64 bits is 4 pixels
	//32-bit pixels: 64 bits is 2 pixels
}


void TEX_LoadTextureFromCHeader(KLBitmap *tex, unsigned short *image, uint16_t width, uint16_t height, KLTextureDepth texture_size, KLTextureFormat texture_format)
{
	int depth;
	switch(texture_size)
	{
		case KL_TEX_DEPTH_8BIT:
			depth = 1;
			break;
		case KL_TEX_DEPTH_16BIT:
			depth = 2;
			break;
		case KL_TEX_DEPTH_32BIT:
			depth = 4;
			break;
		default:
			_64Drive_putstring("Illegal texture size in TEX_LoadTextureFromFS!\n");
			while(true) {};
			break;
	}

	tex->data = image;
	tex->bitdepth = depth;
	tex->size.y = height;
	tex->size.x = width;
	tex->format = texture_format;
	tex->pixel_size = texture_size;
}