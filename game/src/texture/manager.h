#pragma once

// Texture Manager
// For now, the texture manager keeps track of what textures are present in ROM.

#include <libdragon.h>
#include <stdio.h>
#include "kategl/blit/blit.h"

typedef struct texture_list_entry_t {
    struct texture_list_entry_t *next;
    char name[10];
    KLBitmap *info;
} TextureListEntry;

//TextureListEntry *textureList;

#ifdef __cplusplus
extern "C" {
#endif

class TextureManager
{
    private:
    TextureListEntry *list;

    public:
    void AppendToTextureList(TextureListEntry *texture);
    int TextureCount();
    TextureListEntry* GetTextureByName(char *seekname);
};

extern TextureManager textureManager;

// void TM_AppendToTextureList(TextureListEntry **head, TextureListEntry *new_entry);
// int TM_TextureCount(TextureListEntry *list);
// TextureListEntry *TM_GetTextureByName(TextureListEntry *list, char *seekname);

#ifdef __cplusplus
}
#endif
