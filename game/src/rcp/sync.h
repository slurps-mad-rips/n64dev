#pragma once

#include <libdragon.h>
#include "mmio.h"
#include "regs.h"

#ifdef __cplusplus
extern "C" {
#endif

extern uint32_t rsp_active;
extern uint32_t rdp_status;
extern bool waiting_for_rdp;

void wait_for_rsp();
void wait_for_rdp();

#ifdef __cplusplus
}
#endif