#include "rcp/sync.h"

volatile uint32_t rsp_active;
volatile uint32_t rdp_status;
volatile bool waiting_for_rdp;


void wait_for_rsp()
{
	rsp_active = MMIO32(0xA404001C);

	while(rsp_active) 
	{
		rsp_active = MMIO32(0xA404001C);	// Read the RSP semaphore bit, loop until it's clear
	};
}

void wait_for_rdp()
{
	waiting_for_rdp = true;
	rdp_status = MMIO32(0xA410000C);

	while(rdp_status != 0x81) // Wait for RDP DMA to finish.
	{
		rdp_status = MMIO32(0xA410000C);
	}

	waiting_for_rdp = false;
}
