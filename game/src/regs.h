#pragma once

/* Hardware registers. */

// VI
#define VI_REGISTER_BASE 0xA4400000

// SP
#define SP_DMEM_BASE	0xA4000000
#define SP_IMEM_BASE	0xA4001000

// RSP Coprocessor 0 Registers
#define RDP_CMD_START 	0xA4100000 // RDP command buffer START
#define RDP_CMD_END		0xA4100004 // RDP command buffer END
#define RDP_CMD_CURRENT	0xA4100008 // RDP command buffer CURRENT

/* RDP status is special.
	bit 0 R - if 1, use DMEM as source of RDP commands. if 0, use RDRAM as source of RDP commands.
	bit 1 R - RDP is frozen
 	bit 2 R - RDP is flushed
 	bit 3 R - GCLK is alive

	bit 0 W - clear bit 0
	bit 1 W - set bit 0
	bit 2 W - clear bit 1
	bit 3 W - set bit 1
	bit 4 W - clear bit 2
	bit 5 W - set bit 2
	bit 6 W - clear TMEM COUNTER
	bit 7 W - clear PIPE COUNTER
	bit 8 W - clear COMMAND COUNTER
	bit 9 W - clear CLOCK COUNTER
 **/
#define RDP_STATUS		0xA410000C // RDP Status

/* NTSC 320x240 */
static const uint32_t ntsc_320[] = {
    0x00000000, 0x00000000, 0x00000140, 0x00000200,
    0x00000000, 0x03e52239, 0x0000020d, 0x00000c15,
    0x0c150c15, 0x006c02ec, 0x002501ff, 0x000e0204,
    0x00000200, 0x00000400 };

#define RSP_SEMAPHORE	0xA4000A9C

/** 
 * @brief Return the uncached memory address of a cached address
 *
 * @param[in] x 
 *            The cached address
 *
 * @return The uncached address
 */
#define UNCACHED_ADDR(x)    ((void *)(((uint32_t)(x)) | 0xA0000000))

/**
 * @brief Align a memory address to 16 byte offset
 * 
 * @param[in] x
 *            Unaligned memory address
 *
 * @return An aligned address guaranteed to be >= the unaligned address
 */
#define ALIGN_64BYTE(x)     ((void *)(((uint32_t)(x)+63) & 0xFFFFFF40))
