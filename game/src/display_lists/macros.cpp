#include "macros.h"
#include "texture/manager.h"

void DL_MAC_WriteStringToScreen(display_list_t **list, bool load_texture, char *str, int x, int y)
{
	rdp_set_combine_mode(list, CC_C0_RGB_MUL_ZERO_COLOR|CC_C1_RGB_MUL_ZERO_COLOR|CC_C0_RGB_ADD_TEXEL0_COLOR|CC_C1_RGB_ADD_TEXEL0_COLOR);
	rdp_set_other_modes(list, MODE_CYCLE_TYPE_1CYCLE|MODE_CVG_DEST_WRAP);
	rdp_sync(list, SYNC_PIPE);

	texslot_t texture_to_use;

	if(load_texture)
	{
		rdp_load_texture(list, TEXSLOT_0, 0, MIRROR_DISABLED, textureManager.GetTextureByName("FONTBIG")->info);
	}

	for(int i=0; i<strlen(str); i++)
	{
		uint16_t c = str[i] - 0x41;

		int row = (c / 8) * 8;
		int col = (c & 7) * 8;

		texture_to_use = TEXSLOT_0;
		
		rdp_draw_textured_rectangle_scaled(list, texture_to_use, x, y, x+8, y+8, 1.0, 1.0, (col<<5), (row<<5));
		rdp_sync(list, SYNC_TILE);

		x += 8;
	}

}