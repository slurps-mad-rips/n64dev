#include <libdragon.h>
#include "microcode/basic.h"

#ifdef __cplusplus
extern "C" {
#endif

extern void *aligned_zbuffer;

void DL_ClearDepthBuffer(display_context_t context);

#ifdef __cplusplus
}
#endif