#include "depth_clear.h"

void DL_ClearDepthBuffer(display_context_t context)
{
	display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

	rdp_set_default_clipping(&display_list_current);
	rdp_set_default_clipping(&display_list_current);
	rdp_set_default_clipping(&display_list_current);

    // Clear the depth buffer.
	rdp_set_fill_mode(&display_list_current);
	rdp_set_color_image(&display_list_current, RDP_IMAGE_RGBA, RDP_PIXEL_16BIT, 319, ((uint32_t)aligned_zbuffer) & 0x00FFFFFF);
	rdp_set_primitive_color(&display_list_current, 0xFFFCFFFC);
	rdp_draw_filled_rectangle(&display_list_current, 0, 0, 319, 239);

    rdp_sync(&display_list_current, SYNC_FULL);
}