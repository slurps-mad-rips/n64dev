#pragma once

#include <libdragon.h>
#include "kategl/blit/blit.h"
/* macros.h is for display list chunks that are useful embedded in other display lists. */

#ifdef __cplusplus
extern "C" {
#endif

void DL_MAC_WriteStringToScreen(display_list_t **list, bool load_texture, char *str, int x, int y);

#ifdef __cplusplus
}
#endif
