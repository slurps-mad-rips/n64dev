GCCN64PREFIX = mips64-
CHKSUM64PATH = tools/chksum64
MKDFSPATH = tools/mkdfs
HEADERPATH = /usr/local/gcc-mips64vr4300/mips64/lib
N64TOOL = tools/n64tool

LIBKATEGLPATH = $(CURDIR)/../kategl
LIBDRAGONPATH = $(CURDIR)/../libdragon
LIBCPPPATH    = $(CURDIR)/../n64cpp

LINK_FLAGS = -G0 -L$(HEADERPATH) \
								$(LIBCPPPATH)/libn64cpp.a \
								$(LIBDRAGONPATH)/libdragon.a \
								$(LIBKATEGLPATH)/libkategl.a \
								-lc \
								$(LIBDRAGONPATH)/libdragonsys.a \
								-lm \
								-T../n64ld.x

CFLAGS = -std=gnu99 -march=vr4300 -mtune=vr4300 -O0  -I$(CURDIR)/src \ -I$(LIBKATEGLPATH)/include -I$(LIBDRAGONPATH)/include -fpermissive 
ASFLAGS = -mtune=vr4300 -march=vr4300 -I$(CURDIR)/src/microcode

CC = $(GCCN64PREFIX)gcc
AS = $(GCCN64PREFIX)as
LD = $(GCCN64PREFIX)ld

OBJCOPY = $(GCCN64PREFIX)objcopy

ifeq ($(N64_BYTE_SWAP),true)
ROM_EXTENSION = .v64
N64_FLAGS = -b -l 2M -h $(HEADERPATH)/$(HEADERNAME) -o $(PROG_NAME)$(ROM_EXTENSION) $(OBJDIR)/$(PROG_NAME).bin
else
ROM_EXTENSION = .z64
N64_FLAGS = -l 2M -h $(HEADERPATH)/$(HEADERNAME) -o $(PROG_NAME)$(ROM_EXTENSION) $(OBJDIR)/$(PROG_NAME).bin
endif

OBJDIR=./obj
SRCDIR=./src

OBJECTS = 	$(OBJDIR)/hello.o \
			$(OBJDIR)/texture/manager.o \
			$(OBJDIR)/texture/loader.o \
 			$(OBJDIR)/microcode/basic.o \
 			$(OBJDIR)/microcode/overlay_drawtri.o \
			$(OBJDIR)/rsp_overlay.o \
			$(OBJDIR)/display_lists/depth_clear.o \
			$(OBJDIR)/display_lists/macros.o \
			$(OBJDIR)/rcp/sync.o \
 
PROG_NAME = hello

.PHONY: all clean

###
$(OBJDIR)/$(PROG_NAME).elf : $(OBJECTS)
	$(LD) -o $@ $^ $(LINK_FLAGS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) -c $(CFLAGS) -o $@ $^

$(OBJDIR)/texture/%.o: $(SRCDIR)/texture/%.c
	$(CC) -c $(CFLAGS) -o $@ $^

$(OBJDIR)/texture/%.o: $(SRCDIR)/texture/%.m
	$(CC) -c $(CFLAGS) -o $@ $^

$(OBJDIR)/display_lists/%.o: $(SRCDIR)/display_lists/%.cpp
	$(CC) -c $(CFLAGS) -o $@ $^

$(OBJDIR)/rcp/%.o: $(SRCDIR)/rcp/%.cpp
	$(CC) -c $(CFLAGS) -o $@ $^	

$(OBJDIR)/%.o: $(SRCDIR)/%.m
	$(CC) -c $(CFLAGS) -o $@ $^	

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CC) -c $(CFLAGS) -o $@ $^

### Microcode

# Boot
$(OBJDIR)/microcode/basic.bin: $(SRCDIR)/microcode/basic.S
	$(CC) -c -o $(OBJDIR)/microcode/basic_tmp.o $<
	$(OBJCOPY) -O binary -j .text $(OBJDIR)/microcode/basic_tmp.o $(OBJDIR)/microcode/basic.bin

$(OBJDIR)/microcode/basic.o: $(OBJDIR)/microcode/basic.bin
	$(OBJCOPY) -I binary -O elf32-bigmips -B mips4300 --redefine-sym _binary___obj_microcode_basic_bin_start=basic_ucode_start --redefine-sym _binary___obj_microcode_basic_bin_end=basic_ucode_end $(OBJDIR)/microcode/basic.bin $(OBJDIR)/microcode/basic.o

# overlay_drawtri
$(OBJDIR)/microcode/overlay_drawtri.bin: $(SRCDIR)/microcode/overlay_drawtri.S
	$(CC) -c -o $(OBJDIR)/microcode/tmp.o $<
	$(OBJCOPY) -O binary -j .text $(OBJDIR)/microcode/tmp.o $(OBJDIR)/microcode/overlay_drawtri.bin

$(OBJDIR)/microcode/overlay_drawtri.o: $(OBJDIR)/microcode/overlay_drawtri.bin
	$(OBJCOPY) -I binary -O elf32-bigmips -B mips4300 --redefine-sym _binary___obj_microcode_overlay_drawtri_bin_start=overlay_drawtri_ucode_start --redefine-sym _binary___obj_microcode_overlay_drawtri_bin_end=overlay_drawtri_ucode_end $(OBJDIR)/microcode/overlay_drawtri.bin $(OBJDIR)/microcode/overlay_drawtri.o


###
all: kategl $(PROG_NAME)$(ROM_EXTENSION)

clean:
	rm -f *.v64 *.z64 $(OBJDIR)/*.elf $(OBJDIR)/*.o $(OBJDIR)/microcode/*.o $(OBJDIR)/display_lists/*.o $(OBJDIR)/microcode/*.bin $(OBJDIR)/*.bin $(OBJDIR)/texture/*.o

rspsym:
	objdump -t obj/microcode/tmp.o
