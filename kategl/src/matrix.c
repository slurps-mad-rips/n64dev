#include "kategl/matrix.h"

void klUnitMatrix(FX_Matrix44 *m)
{
	m->data[0][0] = 0x00010000;
	m->data[0][1] = 0;
	m->data[0][2] = 0;
	m->data[0][3] = 0;

	m->data[1][0] = 0;
	m->data[1][1] = 0x00010000;
	m->data[1][2] = 0;
	m->data[1][3] = 0;

	m->data[2][0] = 0;
	m->data[2][1] = 0;
	m->data[2][2] = 0x00010000;
	m->data[2][3] = 0;

	m->data[3][0] = 0;
	m->data[3][1] = 0;
	m->data[3][2] = 0;
	m->data[3][3] = 0x00010000;
}
void klMatrixMultiply(FX_Matrix44 *a, FX_Matrix44 *b, FX_Matrix44 *result)
{
	FX_Matrix44 temp;

    temp.data[0][0] = FX_Multiply(a->data[0][0], b->data[0][0]) + FX_Multiply(a->data[0][1], b->data[1][0]) + FX_Multiply(a->data[0][2], b->data[2][0]) + FX_Multiply(a->data[0][3], b->data[3][0]);
	temp.data[0][1] = FX_Multiply(a->data[0][0], b->data[0][1]) + FX_Multiply(a->data[0][1], b->data[1][1]) + FX_Multiply(a->data[0][2], b->data[2][1]) + FX_Multiply(a->data[0][3], b->data[3][1]);
	temp.data[0][2] = FX_Multiply(a->data[0][0], b->data[0][2]) + FX_Multiply(a->data[0][1], b->data[1][2]) + FX_Multiply(a->data[0][2], b->data[2][2]) + FX_Multiply(a->data[0][3], b->data[3][2]);
	temp.data[0][3] = FX_Multiply(a->data[0][0], b->data[0][3]) + FX_Multiply(a->data[0][1], b->data[1][3]) + FX_Multiply(a->data[0][2], b->data[2][3]) + FX_Multiply(a->data[0][3], b->data[3][3]);

    temp.data[1][0] = FX_Multiply(a->data[1][0], b->data[0][0]) + FX_Multiply(a->data[1][1], b->data[1][0]) + FX_Multiply(a->data[1][2], b->data[2][0]) + FX_Multiply(a->data[1][3], b->data[3][0]);
	temp.data[1][1] = FX_Multiply(a->data[1][0], b->data[0][1]) + FX_Multiply(a->data[1][1], b->data[1][1]) + FX_Multiply(a->data[1][2], b->data[2][1]) + FX_Multiply(a->data[1][3], b->data[3][1]);
	temp.data[1][2] = FX_Multiply(a->data[1][0], b->data[0][2]) + FX_Multiply(a->data[1][1], b->data[1][2]) + FX_Multiply(a->data[1][2], b->data[2][2]) + FX_Multiply(a->data[1][3], b->data[3][2]);
	temp.data[1][3] = FX_Multiply(a->data[1][0], b->data[0][3]) + FX_Multiply(a->data[1][1], b->data[1][3]) + FX_Multiply(a->data[1][2], b->data[2][3]) + FX_Multiply(a->data[1][3], b->data[3][3]);
	
    temp.data[2][0] = FX_Multiply(a->data[2][0], b->data[0][0]) + FX_Multiply(a->data[2][1], b->data[1][0]) + FX_Multiply(a->data[2][2], b->data[2][0]) + FX_Multiply(a->data[2][3], b->data[3][0]);
	temp.data[2][1] = FX_Multiply(a->data[2][0], b->data[0][1]) + FX_Multiply(a->data[2][1], b->data[1][1]) + FX_Multiply(a->data[2][2], b->data[2][1]) + FX_Multiply(a->data[2][3], b->data[3][1]);
	temp.data[2][2] = FX_Multiply(a->data[2][0], b->data[0][2]) + FX_Multiply(a->data[2][1], b->data[1][2]) + FX_Multiply(a->data[2][2], b->data[2][2]) + FX_Multiply(a->data[2][3], b->data[3][2]);
	temp.data[2][3] = FX_Multiply(a->data[2][0], b->data[0][3]) + FX_Multiply(a->data[2][1], b->data[1][3]) + FX_Multiply(a->data[2][2], b->data[2][3]) + FX_Multiply(a->data[2][3], b->data[3][3]);

    temp.data[3][0] = FX_Multiply(a->data[3][0], b->data[0][0]) + FX_Multiply(a->data[3][1], b->data[1][0]) + FX_Multiply(a->data[3][2], b->data[2][0]) + FX_Multiply(a->data[3][3], b->data[3][0]);
	temp.data[3][1] = FX_Multiply(a->data[3][0], b->data[0][1]) + FX_Multiply(a->data[3][1], b->data[1][1]) + FX_Multiply(a->data[3][2], b->data[2][1]) + FX_Multiply(a->data[3][3], b->data[3][1]);
	temp.data[3][2] = FX_Multiply(a->data[3][0], b->data[0][2]) + FX_Multiply(a->data[3][1], b->data[1][2]) + FX_Multiply(a->data[3][2], b->data[2][2]) + FX_Multiply(a->data[3][3], b->data[3][2]);
	temp.data[3][3] = FX_Multiply(a->data[3][0], b->data[0][3]) + FX_Multiply(a->data[3][1], b->data[1][3]) + FX_Multiply(a->data[3][2], b->data[2][3]) + FX_Multiply(a->data[3][3], b->data[3][3]);

	for(int i=0; i<4; i++)
	{
		for(int j=0; j<4; j++)
		{
			result->data[i][j] = temp.data[i][j];
		}
	}
}

void FXM44_CopyToDMEM(uint32_t *addr, FX_Matrix44 *m)
{
	// Split the integer and fraction sections up.
	MMIO16((uint32_t)addr+0) = (m->data[0][0] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+2) = (m->data[0][1] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+4) = (m->data[0][2] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+6) = (m->data[0][3] & 0xFFFF0000) >> 16;

	MMIO16((uint32_t)addr+8) = (m->data[1][0] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+10) = (m->data[1][1] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+12) = (m->data[1][2] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+14) = (m->data[1][3] & 0xFFFF0000) >> 16;

	MMIO16((uint32_t)addr+16) = (m->data[2][0] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+18) = (m->data[2][1] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+20) = (m->data[2][2] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+22) = (m->data[2][3] & 0xFFFF0000) >> 16;

	MMIO16((uint32_t)addr+24) = (m->data[3][0] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+26) = (m->data[3][1] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+28) = (m->data[3][2] & 0xFFFF0000) >> 16;
	MMIO16((uint32_t)addr+30) = (m->data[3][3] & 0xFFFF0000) >> 16;

	//
	MMIO16((uint32_t)addr+32) = (m->data[0][0] & 0xFFFF);
	MMIO16((uint32_t)addr+34) = (m->data[0][1] & 0xFFFF);
	MMIO16((uint32_t)addr+36) = (m->data[0][2] & 0xFFFF);
	MMIO16((uint32_t)addr+38) = (m->data[0][3] & 0xFFFF);

	MMIO16((uint32_t)addr+40) = (m->data[1][0] & 0xFFFF);
	MMIO16((uint32_t)addr+42) = (m->data[1][1] & 0xFFFF);
	MMIO16((uint32_t)addr+44) = (m->data[1][2] & 0xFFFF);
	MMIO16((uint32_t)addr+46) = (m->data[1][3] & 0xFFFF);

	MMIO16((uint32_t)addr+48) = (m->data[2][0] & 0xFFFF);
	MMIO16((uint32_t)addr+50) = (m->data[2][1] & 0xFFFF);
	MMIO16((uint32_t)addr+52) = (m->data[2][2] & 0xFFFF);
	MMIO16((uint32_t)addr+54) = (m->data[2][3] & 0xFFFF);

	MMIO16((uint32_t)addr+56) = (m->data[3][0] & 0xFFFF);
	MMIO16((uint32_t)addr+58) = (m->data[3][1] & 0xFFFF);
	MMIO16((uint32_t)addr+60) = (m->data[3][2] & 0xFFFF);
	MMIO16((uint32_t)addr+62) = (m->data[3][3] & 0xFFFF);
}