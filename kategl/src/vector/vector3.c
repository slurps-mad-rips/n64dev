#include "kategl/vector/vector3.h"
#include "kategl/fixed.h"

FX_Vector3 FX_Vector3_FromFloats(float fx, float fy, float fz)
{
	FX_Vector3 v;
	v.x = FX_FromFloat(fx);	
	v.y = FX_FromFloat(fy);	
	v.z = FX_FromFloat(fz);	
	return v;
}