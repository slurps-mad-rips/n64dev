#include "kategl/utility.h"
#include "kategl/matrix.h"
#include "kategl/vector/vector3.h"

Fixed _W;

void klPointMatrixMultiply(FX_Vector3 *out, const FX_Vector3 *in, const FX_Matrix44 *m)
{
	out->x   = FX_Multiply(in->x, m->data[0][0]) + FX_Multiply(in->y, m->data[1][0]) + FX_Multiply(in->z, m->data[2][0]) + /* in->z = 1 */ m->data[3][0]; 
    out->y   = FX_Multiply(in->x, m->data[0][1]) + FX_Multiply(in->y, m->data[1][1]) + FX_Multiply(in->z, m->data[2][1]) + /* in->z = 1 */ m->data[3][1]; 
    out->z   = FX_Multiply(in->x, m->data[0][2]) + FX_Multiply(in->y, m->data[1][2]) + FX_Multiply(in->z, m->data[2][2]) + /* in->z = 1 */ m->data[3][2]; 
    Fixed w  = FX_Multiply(in->x, m->data[0][3]) + FX_Multiply(in->y, m->data[1][3]) + FX_Multiply(in->z, m->data[2][3]) + /* in->z = 1 */ m->data[3][3];

    if(w != 0x00010000)
    {
    	out->x = FX_Divide(out->x, w);
    	out->y = FX_Divide(out->y, w);
    	out->z = FX_Divide(out->z, w);
    }

    _W = w;
}