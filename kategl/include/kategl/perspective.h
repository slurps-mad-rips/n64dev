#include <libdragon.h>
#include "kategl/matrix.h"

#ifdef __cplusplus
extern "C" {
#endif

void klPerspective(FX_Matrix44 *m, float near_plane, float far_plane);

#ifdef __cplusplus
}
#endif