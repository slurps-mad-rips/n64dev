#pragma once

#include "stdint.h"

typedef int32_t Fixed;

typedef struct {
    int16_t x, y;
} Point2D;
Point2D MakePoint(int x, int y);

/** @brief Texture format */
typedef enum
{
    KL_TEX_FORMAT_RGBA,
    KL_TEX_FORMAT_YUV,
    KL_TEX_FORMAT_CI,
    KL_TEX_FORMAT_IA,
    KL_TEX_FORMAT_I
} KLTextureFormat;

typedef enum
{
    KL_TEX_DEPTH_4BIT,
    KL_TEX_DEPTH_8BIT,
    KL_TEX_DEPTH_16BIT,
    KL_TEX_DEPTH_32BIT,
} KLTextureDepth;

  /** @brief Bitmap structure */
typedef struct
{
    /** @brief Dimensions in pixels */
    Point2D size;

    /** 
     * @brief Bit depth expressed in bytes
     *
     * A 32 bit sprite would have a value of '4' here
     */
    uint8_t bitdepth;

    /** 
     * @brief Texture format
     */
    KLTextureFormat format;

    /** 
     * @brief Texture pixel size
     */
    KLTextureDepth pixel_size;

    /** @brief Number of horizontal slices for spritemaps */
    uint8_t hslices;
    /** @brief Number of vertical slices for spritemaps */
    uint8_t vslices;

    /** @brief Start of graphics data */
    void *data;
} KLBitmap;