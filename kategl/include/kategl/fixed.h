/* Fixed-point math. */

#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef int32_t Fixed;

#define FX_Q 16
#define FX_K (1 << (FX_Q-1))

Fixed FX_FromFloat(float f);
Fixed FX_FromInt(int f);
Fixed FX_Add(Fixed a, Fixed b);
Fixed FX_Sub(Fixed a, Fixed b);
Fixed FX_Multiply(Fixed a, Fixed b);
Fixed FX_Divide(Fixed a, Fixed b);

#ifdef __cplusplus
}
#endif
