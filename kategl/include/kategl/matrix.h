#pragma once

/* Fixed-point matrix. */

#include "math.h"
#include "stdint.h"
#include "stdlib.h"
#include "fixed.h"
#include "kategl/utility.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct FX_Matrix44 { Fixed data[4][4]; } FX_Matrix44;

void klUnitMatrix(FX_Matrix44 *m);
void FXM44_CopyToDMEM(uint32_t *addr, FX_Matrix44 *m);
void klMatrixMultiply(FX_Matrix44 *a, FX_Matrix44 *b, FX_Matrix44 *result);

extern void thanksIHateFortran(int *num, int *o);

#ifdef __cplusplus
}
#endif
