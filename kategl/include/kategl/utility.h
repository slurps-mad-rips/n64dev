#pragma once

#define MMIO8(x)  (*(volatile uint8_t *)(x))
#define MMIO16(x) (*(volatile uint16_t *)(x))
#define MMIO32(x) (*(volatile uint32_t *)(x))

struct FX_Vector3;
struct FX_Matrix44;

void klPointMatrixMultiply(struct FX_Vector3 *out, const struct FX_Vector3 *in, const struct FX_Matrix44 *m);

//static Fixed _W = 0;