#pragma once

#include "stdint.h"
#include "kategl/types.h"

void klBitBlt(KLBitmap *destBitmap, Point2D destCoords, KLBitmap *sourceBitmap, Point2D sourceCoords, Point2D blitSize);