#pragma once

#include "kategl/types.h"

typedef struct FX_Vector3 { Fixed x, y, z; } FX_Vector3;

FX_Vector3 FX_Vector3_FromFloats(float fx, float fy, float fz);
