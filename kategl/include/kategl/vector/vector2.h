#pragma once

#include <stdint.h>

/* Vector2f - a vector of two floats. Values are X and Y. */
typedef struct Vector2f {
  float x, y;
} Vector2f;

/* Vector2i - a vector of two ints. */
typedef struct Vector2i {
  uint32_t x, y;
} Vector2i;
