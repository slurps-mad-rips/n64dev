GCCN64PREFIX = mips64-
CHKSUM64PATH = tools/chksum64
MKDFSPATH = tools/mkdfs
HEADERPATH = /usr/local/gcc-mips64vr4300/mips64/lib
N64TOOL = tools/n64tool

HEADERNAME = header

GAMEPATH = $(CURDIR)/game
LIBKATEGLPATH = $(CURDIR)/kategl

CC = $(GCCN64PREFIX)gcc
AS = $(GCCN64PREFIX)as
LD = $(GCCN64PREFIX)ld

OBJCOPY = $(GCCN64PREFIX)objcopy

ifeq ($(N64_BYTE_SWAP),true)
ROM_EXTENSION = .v64
N64_FLAGS = -b -l 2M -h $(HEADERPATH)/$(HEADERNAME) -o $(PROG_NAME)$(ROM_EXTENSION) $(GAMEPATH)/$(OBJDIR)/$(PROG_NAME).bin
else
ROM_EXTENSION = .z64
N64_FLAGS = -l 2M -h $(HEADERPATH)/$(HEADERNAME) -o $(PROG_NAME)$(ROM_EXTENSION) $(GAMEPATH)/$(OBJDIR)/$(PROG_NAME).bin
endif

OBJDIR=./obj
SRCDIR=./src
 
PROG_NAME = hello

.PHONY: all clean

###
all: kategl libdragon $(PROG_NAME)$(ROM_EXTENSION)

$(PROG_NAME)$(ROM_EXTENSION): textures.dfs libn64cpp.a libdragon.a libkategl.a hello.elf
	$(OBJCOPY) $(GAMEPATH)/$(OBJDIR)/$(PROG_NAME).elf $(GAMEPATH)/$(OBJDIR)/$(PROG_NAME).bin -O binary
	rm -f $(PROG_NAME)$(ROM_EXTENSION)
	cd textures; python convert.py; cd ..
	tools/mkdfs textures.dfs textures/tex
	$(N64TOOL) $(N64_FLAGS) -t "Controller Test" -s 1M textures.dfs
	$(CHKSUM64PATH) $(PROG_NAME)$(ROM_EXTENSION)

textures.dfs:
	tools/mkdfs textures.dfs textures/tex

libkategl.a:
	$(MAKE) -C ./kategl

libdragon.a:
	$(MAKE) -C ./libdragon

libn64cpp.a:
	$(MAKE) -C ./n64cpp

hello.elf:
	$(MAKE) -C ./game

clean:
	$(MAKE) -C ./kategl clean
	$(MAKE) -C ./libdragon clean
	$(MAKE) -C ./game clean
	$(MAKE) -C ./n64cpp clean
